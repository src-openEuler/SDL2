Name:           SDL2
Version:        2.30.12
Release:        1
Summary:        Cross-platform multimedia library
License:        zlib and MIT
URL:            https://www.libsdl.org/
Source0:        https://github.com/libsdl-org/SDL/releases/download/release-%{version}/%{name}-%{version}.tar.xz
Source1:        SDL_config.h
Patch0000:      multilib.patch
# Prefer Wayland by default
Patch0001:      SDL2-2.30.1-prefer-wayland.patch

BuildRequires:  alsa-lib-devel audiofile-devel mesa-libGL-devel
BuildRequires:  mesa-libGLU-devel mesa-libEGL-devel libglvnd-devel
BuildRequires:  libXext-devel libX11-devel libXi-devel libXrandr-devel
BuildRequires:  libXrender-devel libXScrnSaver-devel libusb-devel
BuildRequires:  libXinerama-devel libXcursor-devel systemd-devel
%ifarch loongarch64
BuildRequires:  pkgconfig(libpulse-simple) 
%else
BuildRequires:  pkgconfig(libpulse-simple) pkgconfig(jack)
%endif
BuildRequires:  pkgconfig(dbus-1) pkgconfig(ibus-1.0)
BuildRequires:  pkgconfig(wayland-client) pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-cursor) pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-scanner) pkgconfig(xkbcommon)
BuildRequires:  vulkan-devel mesa-libgbm-devel libdrm-devel
BuildRequires:  cmake-rpm-macros

%description
Simple DirectMedia Layer (SDL) is a cross-platform multimedia library designed
to provide fast access to the graphics frame buffer and audio device.

%package devel
Summary:        Files needed to develop Simple DirectMedia Layer applications
Requires:       %{name} = %{version}-%{release}
Requires:       mesa-libEGL-devel mesa-libGLES-devel libX11-devel
Provides:       %{name}-static = %{version}-%{release}
Obsoletes:      %{name}-static < %{version}-%{release}

%description devel
Simple DirectMedia Layer (SDL) is a cross-platform multimedia library designed
to provide fast access to the graphics frame buffer and audio device. This
package provides the libraries, include files, and other resources needed for
developing SDL applications.

%package static
Summary:        Static libraries for SDL2

%description static
Static libraries for SDL2.

%prep
%autosetup -p1
sed -i -e 's/.*AM_PATH_ESD.*//' configure.ac

%build
%configure \
    --enable-sdl-dlopen --enable-video-kmsdrm \
    --disable-arts --disable-esd --disable-nas \
    --enable-pulseaudio-shared --enable-jack-shared \
    --enable-alsa --enable-video-wayland \
    --enable-video-vulkan --enable-sse2=no \
    --enable-sse3=no --disable-rpath \
%ifarch loongarch64
    --enable-lasx=no --enable-lsx=no
%endif

%make_build

%install
%make_install
%delete_la

mv %{buildroot}%{_includedir}/SDL2/SDL_config.h %{buildroot}%{_includedir}/SDL2/SDL_config-%{_arch}.h
install -pm 0644 %{SOURCE1} %{buildroot}%{_includedir}/SDL2/SDL_config.h

%files
%doc BUGS.txt CREDITS.txt README-SDL.txt
%{_libdir}/lib*.so.*

%files devel
%doc TODO.txt WhatsNew.txt
%{_bindir}/*-config
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/sdl2.pc
%{_libdir}/cmake/SDL2/
%{_includedir}/SDL2
%{_datadir}/aclocal/*

%files static
%{_libdir}/lib*.a

%changelog
* Mon Feb 03 2025 Funda Wang <fundawang@yeah.net> - 2.30.12-1
- update to 2.30.12

* Fri Jan 17 2025 wangkai <13474090681@163.com> - 2.30.11-1
- Upgrade version to 2.30.11

* Fri Mar 08 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 2.30.0-2
- disable lsx for lasx loongarch64

* Wed Feb 28 2024 xu_ping <707078654@qq.com> - 2.30.0-1
- Upgrade version to 2.30.0

* Thu Oct 19 2023 xu_ping <707078654@qq.com> - 2.28.4-1
- Upgrade version to 2.28.4

* Mon Apr 24 2023 panchenbo <panchenbo@kylinsec.com.cn> - 2.0.12-7
- add sw_64 support

* Thu Mar 02 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 2.0.12-6
- add loongarch support

* Tue Jan 10 2023 jiangpeng <jiangpeng01@ncti-gba.cn> - 2.0.12-5
- fix CVE-2022-4743

* Mon Apr 11 2022 yaoxin <yaoxin30@h-partners.com> - 2.0.12-4
- Fix CVE-2020-14409 CVE-2020-14410

* Tue Mar 15 2022 yuanxin <yuanxin24@h-partners.com> - 2.0.12-3
- Type:CVE
- ID:NA
- SUG:NA
- DESC:Fix CVE-2021-33657

* Sat Jan 8 2022 zhouwenpei <zhouwenpei1@huawei.com> - 2.0.12-2
- Fix build against wayland

* Mon Nov 16 2020 Zhiyi Weng <zhiyi@iscas.ac.cn> - 2.0.12-1
- Update to 2.0.12

* Wed Aug 6 2020 xinghe<xinghe1@huawei.com> - 2.0.8-10
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:replace mesa-libELES-devel to libglvnd-devel

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.0.8-9
- Type:CVE
- ID:NA
- SUG:NA
- DESC:Fix CVE-2019-13616

* Mon Feb 24 2020 yuxiangyang <yuxiangyang4@huawei.com> - 2.0.8-8
- Delete buildrequires for jack-audio-connection-kit

* Fri Nov 29 2019 lijin Yang <yanglijin@huawei.com> - 2.0.8-7
- Package Init
